import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Select {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		Connection conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/oneDB","SA","Passw0rd");
		Statement stmt=conn.createStatement();
		ResultSet rs= stmt.executeQuery("select id,name,address from user");
		
		while (rs.next()) {
			String id= rs.getString("id");
			String name= rs.getString("name");
			String address= rs.getString("address");
			System.out.println("ID: "+id+", Name: "+name+", Adress: "+address);
		}
	}

}
